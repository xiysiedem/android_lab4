package krzysztofgrys.lab4xd;


import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class Listaa extends ListFragment  implements AdapterView.OnItemClickListener {
    String[] lista ;
    String[] lista1;
    ArrayList<Car> cars;

    public Listaa() {

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        cars = new ArrayList<>();
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
      // TO NIE WAZNE

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public void onResume(){
        super.onResume();
        readFile();
        readFilecars();

        if(lista!=null){
            ListAdapter adapter = new ListAdapter(getActivity(),cars);
            ListView listView = (ListView) getActivity().findViewById(R.id.listaxD);
            listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    Toast.makeText(getContext(),
                            "Wybrales " + cars.get(position).getMarka(), Toast.LENGTH_SHORT)
                            .show();
                }
            });
        }
    }

    public void readFile(){
        try{
            InputStream instream = getContext().openFileInput("trucks.txt");

            if (instream != null){
                InputStreamReader inputreader = new InputStreamReader(instream);
                BufferedReader buffreader = new BufferedReader(inputreader);
                String line="";
                try {
                    int i=0;

                    while ((line = buffreader.readLine()) != null) {
                        lista = new String[1000];
                        lista = line.split(" ");
                        cars.add(new Car(i,lista[0],lista[1],"ciezarowka"));
                        i++;
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
                inputreader.close();
                buffreader.close();
                instream.close();
            }
        }
        catch (Exception e){
            Log.v("FileTrucks",e.getMessage());
        }
    }
    public void readFilecars(){
        try{
            InputStream instream = getContext().openFileInput("cars.txt");

            if (instream != null){
                InputStreamReader inputreader = new InputStreamReader(instream);
                BufferedReader buffreader = new BufferedReader(inputreader);
                String line="";
                try{
                    int i=0;
                    while ((line = buffreader.readLine()) != null) {
                        lista1 = new String[10000];
                        lista1 = line.split(" ");
                        cars.add(new Car(i,lista1[0],lista1[1],"samochod"));
                        i++;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                inputreader.close();
                buffreader.close();
                instream.close();
            }
        }
        catch (Exception e){
            Log.v("FileCars",e.getMessage());
        }
    }
    public interface ref{
        void onResume();
    }
}
