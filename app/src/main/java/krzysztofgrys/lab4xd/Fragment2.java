package krzysztofgrys.lab4xd;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;


public class Fragment2 extends Fragment implements RadioGroup.OnCheckedChangeListener {

    FragmentManager fragmentManager;
    FragmentTransaction mTransaction;

    public Fragment2() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragment2, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((RadioGroup) getActivity().findViewById(R.id.opcje)).setOnCheckedChangeListener(this);
        fragmentManager = getFragmentManager();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        mTransaction = fragmentManager.beginTransaction();
        switch (group.getCheckedRadioButtonId()) {
            case R.id.radio1:
                mTransaction.replace(R.id.fragment_holder, new AddCar(), "Truck");
                mTransaction.commit();
                break;
            case R.id.radio2:
                mTransaction.replace(R.id.fragment_holder, new AddTruck(), "Car");
                mTransaction.commit();
                break;
        }
    }


}
