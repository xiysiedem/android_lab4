package krzysztofgrys.lab4xd;

public class Car {
    private int id;
    private String model;
    private String marka;
    private String silnik;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public String getSilnik() {
        return silnik;
    }

    public void setSilnik(String silnik) {
        this.silnik = silnik;
    }

    public Car(int id,String model, String marka, String silnik) {
        this.id = id;
        this.model = model;
        this.marka = marka;
        this.silnik = silnik;
    }

    public String toString(){
        return  id+" "+model+" "+marka+" "+silnik;
    }
}
