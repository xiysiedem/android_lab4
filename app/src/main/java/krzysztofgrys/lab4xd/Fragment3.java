package krzysztofgrys.lab4xd;


import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class Fragment3 extends Fragment {
    FragmentManager fragmentManager;
    FragmentTransaction mTransaction;


    public Fragment3() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_fragment3, container, false);
    }
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        fragmentManager = getFragmentManager();

        Configuration config = getResources().getConfiguration();

        if(config.orientation == 1) {
            mTransaction = fragmentManager.beginTransaction();
            mTransaction.replace(R.id.fragment3_holder, new Listaa(), "listas");
            mTransaction.addToBackStack("listaa");
            mTransaction.commit();
        }else{

            mTransaction = fragmentManager.beginTransaction();
            mTransaction.replace(R.id.fragment3_holder, new Listaa(), "listas1");
            mTransaction.replace(R.id.fragment3_folderxD, new Details(), "details");

            mTransaction.addToBackStack("listas1");
            mTransaction.addToBackStack("details");
            mTransaction.commit();

        }
    }
    public void onResume() {
        super.onResume();
    }




}
