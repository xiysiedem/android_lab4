package krzysztofgrys.lab4xd;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;


public class ListAdapter extends ArrayAdapter<Car> {


    public ListAdapter(Context context, ArrayList<Car> cars) {
        super(context, 0, cars);

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Car car = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list, parent, false);
        }

        TextView marka = (TextView) convertView.findViewById(R.id.marka);
        TextView model = (TextView) convertView.findViewById(R.id.model);
        TextView id = (TextView) convertView.findViewById(R.id.idxD) ;

        id.setText(""+car.getId()+" ");
        model.setText(car.getMarka()+" ");
        marka.setText(car.getSilnik()+" ");

        return convertView;
    }
}
