package krzysztofgrys.lab4xd;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStreamWriter;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddCar extends Fragment {


    public AddCar() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_car, container, false);

    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Button save = (Button) getView().findViewById(R.id.dodaj);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText model = (EditText) getView().findViewById(R.id.model);
                EditText marka = (EditText) getView().findViewById(R.id.marka);
                EditText rodzaj = (EditText) getView().findViewById(R.id.capacity);


                writeToFile(model.getText().toString() + " " + marka.getText().toString() + " " + rodzaj.getText().toString() + " " + "auto" + "\n");
                Toast.makeText(getContext(),"Dodano samochod", Toast.LENGTH_SHORT) .show();
            }
        });


    }

    private void writeToFile(String data) {
        try {

            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(getContext().openFileOutput("cars.txt", Context.MODE_APPEND));
            outputStreamWriter.write(data);
            outputStreamWriter.close();

        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }

    }
}
