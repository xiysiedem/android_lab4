package krzysztofgrys.lab4xd;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;


public class Fragment1 extends Fragment {
    int cars = 0;
    int trucks = 0;

    public Fragment1() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_fragment1, container, false);
    }

    public void onResume() {
        super.onResume();

        readCars();
        readTrucks();
        TextView statistic_cars = (TextView) getActivity().findViewById(R.id.statystyki_cars);
        TextView statistic_truck = (TextView) getActivity().findViewById(R.id.statystyki_trucks);
        statistic_cars.setText("Samochody: "+cars);
        statistic_truck.setText("Cięzarówki: "+trucks);
    }

    public void deletefiles(){
        File dir = getActivity().getFilesDir();
        File file = new File(dir, "ciez.txt");
        file.delete();
        file = new File(dir,"carss.txt");
        file.delete();
    }

    public interface refre{
        void onResume();
    }


    public void readCars() {
        try {
            InputStream instream = getActivity().openFileInput("trucks.txt");

            if (instream != null) {
                InputStreamReader inputreader = new InputStreamReader(instream);
                BufferedReader buffreader = new BufferedReader(inputreader);
                try {
                    trucks = 0;
                    while (( buffreader.readLine()) != null) {
                        trucks++;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                inputreader.close();
                buffreader.close();
                instream.close();
            }
        } catch (Exception e) {
            e.getMessage();

        }
    }

    public void readTrucks() {
        try {
            InputStream instream = getActivity().openFileInput("cars.txt");
            if (instream != null) {
                InputStreamReader inputreader = new InputStreamReader(instream);
                BufferedReader buffreader = new BufferedReader(inputreader);
                try {
                    cars = 0;
                    while ((buffreader.readLine()) != null) {
                        cars++;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                inputreader.close();
                buffreader.close();
                instream.close();
            }
        } catch (Exception e) {
             e.getMessage();

        }
    }
}